# Pumpkin Spice

## Ingredients

- 1.5 T cinnamon (~3 sticks).
- 1 t ginger
- 1 t nutmeg
- 3/4 t allspice
- 3/4 t cloves

## Ingredients by weight (double)

- 0.05 lb cinnamon
- 0.01 lb ginger
- 0.01 lb nutmeg
- 0.01 lb allspice
- 0.01 lb cloves

## Directions

1. Finely grind all spices and mix thoroughly.
