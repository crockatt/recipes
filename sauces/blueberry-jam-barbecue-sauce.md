# Blueberry Jam Barbecue Sauce

## Ingredients

- 8 oz blueberry jam
- 1/4 C balsamic vinegar
- 1/4 C Worcestershire sauce
- 1/4 C chili sauce
- 1/4 C rye or bourbon whiskey
- 1/3 C chopped onion
- 2 cloves garlic
- 2 T dark brown sugar
- 1 T dijon mustard
- 1 T butter
- 1 t mesquite liquid smoke
- 1 t ancho pepper
- 1/4 t allspice
- salt
- pepper

## Directions

1. Pulse ingredients in blender until smooth.
1. Pour into saucepan and simmer on low until thickend and smooth.
1. Allow to cool. Keeps in fridge about 1 week.

