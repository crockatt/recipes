
SHELL := /bin/bash

subdirs := beverages desserts meals sauces
.PHONY: $(subdirs)

all: $(subdirs)

$(subdirs):
	if [ ! -e $@/makefile ]; then ln -sv ../makefile.sub $@/makefile; fi
	make -C $@

clean:
	rm -v */*.{tex,aux,log,pdf}
