# Aged Eggnog

## Ingredients

- 12 large egg yolks
- 1 lb sugar
- 1 pint whole milk
- 1 pint half-and-half
- 1 pint heavy cream
- 1 C Jamaican rum
- 1 C cognac
- 1 C bourbon
- 1 t grated nutmeg
- 1/4 t salt

## Directions

1. Gradually beat sugar and nutmeg into egg yolks until lightened in color and falling in ribbons.
1. Combine dairy, booze, and salt in a separate bowl, then slowly beat into the egg mixture.
1. Pour into glass containers and store in the refrigerator for at least two weeks, ideally 2-3 months or longer.
1. Makes (4) 32 oz bottles.
