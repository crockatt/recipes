# Eggnog

## Ingredients

- 4 egg yolks
- 1/3 C + 1 T sugar
- 1 pint whole milk
- 1 C heavy cream
- 3 oz bourbon
- 1 t nutmeg
- 4 egg whites

## Directions (raw)

1. Beat egg yolds until they lighten in color. Gradually add 1/3 C sugar and continue to beat until completely dissolved. Add milk, cream, bourbon and nutmeg: stir to combine.
1. Beat egg whites to soft beaks. Gradually add 1 T sugar while continuing beating until stiff peaks form.
1. Whisk egg whites into mixture. Chill and serve.

## Directions (cooked)

1. Beat egg yolks until they lighten in color. Gradually add 1/3 C sugar and continue to beat until completely dissolved. Set aside.
1. In a medium saucepan over high heat, combine milk, cream, nutmeg, and bring just to a boil, stirring occasionally. Remove from heat and gradually temper the hot mixture into the egg and sugar mixture. Return everything to the pot and heat until reaching 160 degrees F. Remove from heat, stir in bourbon, pour into a medium mixing bowl, and set in the refrigerator to chill.
1. Beat egg whites to soft beaks. Gradually add 1 T sugar while continuing beating until stiff peaks form.
1. Whisk egg whites into chilled mixture.
