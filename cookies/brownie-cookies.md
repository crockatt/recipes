# Brownie Cookies

## Ingredients

- 90 g dark couverture chocolate
- 45 g unsalted butter
- 50 g egg
- 52.5 g brown sugar
- 1 g salt
- 2 g vanilla extract
- 43 g flour
- 7 g unsweetened cocoa powder
- 1.5 g baking powder
- 0.5 g baking soda

# Directions

1. Melt chocolate and butter in double boiler, stirring until melted and smooth.
1. Whisk together egg, brown sugar, and salt. Beat on high speed for 3 minutes, until smooth and light in color.
1. Gently whisk in the chocolate mixture (still warm). Add the vanilla extract and incorporate.
1. Sift in the flour, cocoa powder, baking powder, and baking soda. Fold in gently, being careful not to over-mix.
1. Chill in fridge for 5 minutes (until batter thickens slightly): do not over-chill.
1. Arrange on baking sheet using 5cm scoop (flattened on top).
1. Bake for 10-13 minutes at 180 degrees C.

