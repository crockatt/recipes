# Oatmeal Molasses Cookies

## Ingredients

- 1 C butter
- 1 + 3/4 C brown sugar
- 1 t vanilla
- 1 egg
- 1/4 C molasses

- 2 C flour
- 1 + 1/2 C oats
- 1 t salt
- 1 t baking soda
- 1/2 t baking powder
- 1 t cinnamon

## Directions

1. Cream together butter, sugar, and vanilla.
1. Incorporate egg and then molasses into butter mixture.
1. Combine dry ingredients, then mix into the butter mixture.
1. Bake 1 inch balls for 10-12 minutes at 350 degrees.
1. If desired, top with dusting of powdered sugar immediately after removing from oven.
