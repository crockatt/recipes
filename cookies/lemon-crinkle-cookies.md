# Lemon Crinkle Cookies

## Ingredients

- 4 C flour
- 2 t corn starch
- 1 t baking soda
- 1 t baking powder
- 1 t salt
- 1 C unsalted butter
- 1 1/4 C sugar
- 2 large eggs + 2 large yolks
- 3/4 t vanilla extract
- 3/4 t lemon extract
- 1/4 C lemon juice
- 2 t lemon zest

# Directions

1. Whisk together flour, corn starch, baking soda, baking powder, salt.
1. In another bowl, cream the butter for ~30 seconds. Add the sugar and mix until light and fluffy (2-4 minutes).
1. Blend in eggs and yolks, extracts, and lemon juice and zest.
1. Gradually add in flour mixture until combined.
1. Roll into balls using 5cm scoop. Roll in granuated sugar, followed by powdered sugar.
1. Bake 10-12 minutes at 400 degrees F.

