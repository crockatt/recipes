# Recipes

## Meals

[Chicken & Rice](meals/chicken-and-rice.md)

[Macaroni & Cheese](meals/macaroni-and-cheese.md)

[Risotto](meals/risotto.md)

## Desserts

[Cannoli Cheesecake](desserts/cannoli-cheesecake.md)

[Carrot Cake](desserts/carrot-cake.md)

[Cranberry Orange Jello](desserts/cranberry-orange-jello.md)

[Oatmeal Date Squares](desserts/oatmeal-date-squares.md)

[Panna Cotta](desserts/panna-cotta.md)

[Pumpkin Ricotta Cheesecake](desserts/pumpkin-ricotta-cheesecake.md)

[Whipped Cream](desserts/whipped-cream.md)

## Cookies

[Brownie Cookies](cookies/brownie-cookies.md)

[Lemon Crinkle Cookies](cookies/lemon-crinkle-cookies.md)

[Oatmeal Molasses Cookies](cookies/oatmeal-molasses-cookies.md)

## Sauces, Spices, etc.

[Blueberry Jam Barbecue Sauce](sauces/blueberry-jam-barbecue-sauce.md)

[Pumpkin Spice](sauces/pumpkin-spice.md)

## Beverages

[Aged Eggnog](beverages/aged-eggnog.md)

[Eggnog](beverages/eggnog.md)
