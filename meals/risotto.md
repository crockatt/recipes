# Risotto

## Ingredients

- 8 C chicken broth
- 2 C arborio rice
- 1 C dry white wine (eg. sauvignon blanc)
- olive oil
- medium onion, finely diced
- garlic
- 1 t MSG
- 6 T butter
- 1/2 C parmesan cheese

## Directions

1. Heat broth to a low simmer in a large pot.
1. Cook garlic and onion in olive oil.
1. Stir in rice and MSG. Cook rice briefly, until golden.
1. Add wine and stir until absorbed.
1. Add broth in approximately half cup increments, stirring constantly each time until fully absorbed.
1. Remove from heat and stir in butter and parmesan cheese until fully incorporated.
1. May serve topped with sauteed mushrooms, etc. as desired.

