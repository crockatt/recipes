# Macaroni & Cheese

## Ingredients

- 24 oz pasta noodles
- 2 lbs extra sharp cheddar cheese
- 0.5 lbs Gruyere cheese
- 6 T butter
- 6 T flour
- 2 C (1 pint, 16 fl oz) heavy cream
- 1 C water
- 2 t salt
- 2 t MSG

## Directions

1. Grate cheeses into separate bowls.
1. Use the butter and flour to make a roux.
1. Now make the beshamel sauce: Mix cream and water before whisking into the roux along with salt and MSG. Whisk constantly over medium-low heat until thickened.
1. Gradually melt in the cheese.
1. Cook and drain the noodles. Return noodles to the pot and add the cheese sauce.

