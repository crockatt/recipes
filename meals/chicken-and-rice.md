# Chicken & Rice

## Ingredients

- 2 C chicken stock
- 1/2 C white wine
- 1/2 C cream
- 2 T corn starch
- mushrooms, sliced
- 1 onion, finely chopped
- garlic
- 1 C rice
- salt & pepper
- 4 chicken thighs
- 0.2 lbs grated Parmesan cheese

## Directions

Given recipe fits standard 12 inch cast iron skillet. Double recipe is suitable for 17 cast iron skillet.

1. Preheat oven to 350 degrees.
1. Heat onions and mushrooms in greased cast iron skillet over medium high heat. After mushrooms release their moisture, add garlic. Stir and cook until extra moisture has cooked off.
1. Add rice and combine.
1. Whisk together stock, wine, cream, corn starch, and salt. Add to pan, stir to combine.
1. Add chicken and salt exposed skins.
1. Bake until rice absorbs moisture.
1. Remove from oven and extract chicken from pan. Stir in cheese and pepper.
