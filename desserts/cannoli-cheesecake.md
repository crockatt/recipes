# Cannoli Cheesecake

## Ingredients

### Crust

- 7 oz package cannoli shells
- 3 T unsalted butter, melted
- 2 T sugar

### Filling 

- 4 C ricotta cheese
- 1 + 1/2 C sugar
- 1/4 C flour
- 1/2 C whipping cream
- 2 t vanilla extract
- 1 t orange zest
- 5 eggs

### Toppings (optional)

- 1/3 C mini chocolate chips
- Whipped cream
- Confectionary sugar

## Directions

1. Preheat oven to 350 degrees.
1. Crush cannoli shells. Combine with butter and sugar.
1. Bake 10 minuts, then cool.
1. Mix ricotta, sugar, flour. Add whipping cream, vanilla, and zest. Add eggs, one at a time, mixing just until blended after each addition.
1. Pour filling over crust and bake for 1 hour 15 minutes.
1. Sprinkle top with chocolate chips, pressing in lightly. Bake 10 more minuts.
1. Let cool and refrigerate at least 7 hours.

