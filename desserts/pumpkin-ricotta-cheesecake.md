# Pumpkin Ricotta Cheesecake

## Ingredients

### Crust

- 8 oz graham crackers
- 3 T unsalted butter, melted
- 2 T sugar
- 1 t pumpkin spice

### Filling 

- 30 oz ricotta
- 8 oz mascarpone
- 1 + 1/2 C sugar
- 1 T molasses
- 1/4 C flour
- 1/3 C cream
- 15 oz can pumpkin
- 2 t vanilla
- 1 T + 1 t pumpkin spice

## Directions

1. Preheat oven to 350 degrees.
1. Combine crust ingredients in food processor. Pour into pan and gently flatten (do not compress).
1. Bake 10 minuts, then cool.
1. Mix together filling. Add eggs last, one at a time, mixing until just blended after each addition.
1. Pour filling over crust and bake for about 1 hour 40 minutes.
1. Let cool and refrigerate.
1. May be topped with pumpkin spice whipped cream.

## Notes

1. One batch of pumpkin spice makes one cheesecake with enough spice leftover for one batch of pumpkin spice whipped cream.

