# Whipped Cream

## Ingredients

### Base

- 2 C (16 oz) heavy cream
- 1/4 C powdered sugar
- 1/4 C heavy cream powder
- vanilla

### Optional additions

- 75 g chocolate, frozen and finely chopped
- 2 t pumpkin spice

## Directions

1. Whip cream until thickend.
1. Add vanilla (and spice additions).
1. Gradually incorporate powdered sugar and heavy cream powder.
1. Briefly whip in additions (eg., chocolate chips).

