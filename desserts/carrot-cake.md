# Carrot Cake

## Ingredients

### Cake

- 3 C grated carrot
- 4 eggs
- 2 C sugar
- 1 + 1/3 C vegetable oil
- 2 C flour
- 2 t baking soda
- 2 t baking powder
- 2 t cinnamon
- 1 t salt

### Frosting

- 1 lb cream cheese
- 1/2 lb unsalted butter
- Confectioner's sugar as needed

## Directions

1. Combine wet and dry separately. Combine mixtures. Let sit 15 minutes.
1. Bake in buttered and floured 10" pans at 350 degrees, approximately 20-30 minutes.
1. Let cool in pan 10 minutes, then remove from pan to finish cooling.
1. For the frosting, beat together cream cheese and butter. Add sugar to desired sweetness.