# Cranberry Orange Jello

## Ingredients

- 2 packages (small) strawberry (or strawberry-banana) jello
- 1 C hot water
- 1 jar (12 to 16 oz.) cranberry relish (substitute cranberry sauce or other if relish cannot be found)
- 1 C (8 fl. oz.) crushed pineapple (include juice)
- 3 mashed bananas

| Item | Standard Batch | 1.5 Batch |
| --- | --- | --- |
| jello | 2 small packages | 3 small packages |
| hot water | 1 C | 1.5 C |
| cranberry | 12-16 oz | 18-24 oz |
| pineapple | 8 oz | 12 oz |
| bananas | 3 | 4-5 |

## Directions

1. Mix thoroughly.
1. Place in mold.
1. May include 0.5 to 1.0 pint sour cream layer.
1. Fits 6 C (48 fl. oz.) mold.

