# Panna Cotta

## Ingredients

- 2 + 1/4 tsp gelatin
- 2 C (16 oz) heavy cream
- 1/2 C sugar
- 1.5 tsp vanilla

## Directions

1. Dissolve gelatin in 1 T of cream, for ~5 minutes.
1. Combine remaining ingredients over medium heat. Stir occasionally until sugar dissolves and bubbles appear around edge of pan.
1. Remove from heat, add gelatin, stir until combined.
1. Pour into molds.
1. Chill for at least 4 hours to set.

