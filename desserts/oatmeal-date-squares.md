# Oatmeal Date Squares

## Ingredients

### Dough

- 3/4 C butter
- 1 C brown sugar (packed)
- 1 + 3/4 C flour
- 1 t salt
- 1/2 t baking soda
- 1 + 1/2 C oats

### Filling

- 1 + 1/2 lbs dates (chopped)
- 1/3 C sugar
- 1 + 1/2 C water
- 1 t vanilla

## Directions

1. Combine dates, sugar, and water in a medium saucepan. Bring to a boil, then reduce heat and simmer for
   10-12 minutes, stirring frequently.
1. Remove from heat, stir in vanilla, and let cool for 5-10 minutes.
1. Cream together butter and brown sugar. 
1. Separately, combine flour, salt, and baking soda.
1. Add flour mixture to butter mixture, combine well, and then mix in oats.
1. Press half of the crust mixture into a 10-inch square pan. Spread date mixture over crust, and then top with
   remaining crust mixture.
1. Bake at 375 degrees for 25-30 minutes, or until browned.
